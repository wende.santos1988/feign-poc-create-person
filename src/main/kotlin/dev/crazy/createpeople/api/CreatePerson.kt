package dev.crazy.createpeople.api

import dev.crazy.createpeople.domain.services.CreatePersonService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/create")
class CreatePerson(
    private val createPersonService: CreatePersonService
) {

    @GetMapping("person/{token}")
    fun createPerson(@PathVariable token: String) {
        createPersonService.createOneThousand(token)
    }

}