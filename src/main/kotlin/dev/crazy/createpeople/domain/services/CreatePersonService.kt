package dev.crazy.createpeople.domain.services

import dev.crazy.createpeople.clients.feign.CreatePersonClient
import org.springframework.stereotype.Service

@Service
class CreatePersonService(
    private val createPersonClient: CreatePersonClient
) {
    fun createOneThousand(token: String) {
        print("createPerson => ${createPersonClient.createPerson(token)}")
    }
}