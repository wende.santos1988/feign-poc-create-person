package dev.crazy.createpeople.domain.entities

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class Person (

    @Id
    val uuid: String,

    @Column
    val name: String,

    @Column
    val cpf: String,

    @Column
    val email: String
)