package dev.crazy.createpeople.clients.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class PersonDTO(

    @JsonProperty("uuid")
    val uuid: String,

    @JsonProperty("name")
    val name: String,

    @JsonProperty("cpf")
    val cpf: String,

    @JsonProperty("email")
    val email: String
)