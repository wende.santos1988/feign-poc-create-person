package dev.crazy.createpeople.clients.feign

import dev.crazy.createpeople.clients.dto.PersonDTO
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(name = "create-person-service", url = "https://api.invertexto.com/v1")
interface CreatePersonClient {

    @GetMapping("faker?token={token}")
    fun createPerson(@PathVariable token: String): PersonDTO
}